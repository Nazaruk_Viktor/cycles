public class Cycles {
	public static void main(String[] args) {
		int f1 = 1;
		int f2 = 1;
		int counter = 0;
		while(counter < 11) {
			if(counter % 2 == 0) {
				System.out.print(f1 + " ");
				f1 += f2;
			}else {
				System.out.print(f2 + " ");
				f2 += f1;
			}
			counter++;
		}
		System.out.println();
		f1 = 1;
		f2 = 1;
		for(int i = 0; i < 11; i++) {
			if(i % 2 == 0) {
				System.out.print(f1 + " ");
				f1 += f2;
			}else {
				System.out.print(f2 + " ");
				f2 += f1;
			}
		}
	} 
}
